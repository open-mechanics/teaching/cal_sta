function fe_int = element_forces_frame(Ee, Ae, Ie, xz_a, xz_b, de_loc, fe_loc)

%% Geometry
Dxz = xz_b - xz_a; % Coordinate differences
Le = norm(Dxz); % Element length

%% Normal forces
fe_int = zeros(6,1); % Internal forces at element nodes

ne = Ee*Ae/Le; % Normal stifness

fe_int(1) = -fe_loc(1) + ne*(de_loc(4) - de_loc(1));
fe_int(4) =  fe_loc(4) + ne*(de_loc(4) - de_loc(1));

%% Membrane forces (shear and bending moments)
ke = 2*Ee*Ie/Le; % Bending stiffness
psie = (de_loc(5) - de_loc(2))/Le; % Auxiliary factor

fe_int(2) = -fe_loc(2) + 3*ke/Le*( de_loc(3) + de_loc(6) + 2*psie );
fe_int(3) = -fe_loc(3) - ke*( 2*de_loc(3) + de_loc(6) + 3*psie);
fe_int(5) =  fe_loc(5) + 3*ke/Le*( de_loc(3) + de_loc(6) + 2*psie );
fe_int(6) =  fe_loc(6) + ke*( de_loc(3) + 2*de_loc(6) + 3*psie);
end