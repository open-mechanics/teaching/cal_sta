function [Ke, Te] = stiffness_matrix_frame(Ee, Ae, Ie, xz_a, xz_b)

%% Geometry
Dxz = xz_b - xz_a; % Coordinate differences
Le = norm(Dxz); % Element length
c = Dxz(1) / Le; s = Dxz(2) / Le; % Directional cosines

%% Local stiffness matrix
Ke_loc = zeros(6,6);

Ke_loc([1,4], [1,4]) = Ee*Ae/Le * ... % Membrane stiffness
[ 1, -1; 
 -1,  1 ];

Ke_loc([2,3,5,6], [2,3,5,6]) = 2*Ee*Ie/Le * ... % Bending stiffness
[ 6/(Le*Le), -3/Le, -6/(Le*Le), -3/Le;  
      -3/Le,     2,       3/Le,     1;
 -6/(Le*Le),  3/Le,  6/(Le*Le),  3/Le;
      -3/Le,     1,       3/Le,     2 ];

%% Transformation matrix
Te = zeros(6, 6);

Te(1:3, 1:3) = ... % Nodal coordinate transformation
    [ c, s, 0; 
     -s, c, 0; 
      0, 0, 1 ]; 

Te(4:6, 4:6) = Te(1:3, 1:3); % Nodal coordinate transformation 

%% Global stiffness matrix
Ke = Te'*Ke_loc*Te;
end