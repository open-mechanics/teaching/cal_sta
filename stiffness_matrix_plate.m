function Ke = stiffness_matrix_plate(Ee, nue, xye, te)

%% Geometry
Lx = xye(2,1) - xye(1,1);
Ly = xye(3,2) - xye(2,2);

%% Bending stiffness matrix
D_b = Ee*te^3/(12*(1 - nue^2)) * ...
    [  1, nue,         0; 
      nue,  1,         0; 
        0,  0, (1-nue)/2];

%% Integration of stiffness matrix
Ke = zeros(12, 12);

xi_g  = [-1;  1;  1; -1] / sqrt(3);  % Local coordindates of Gauss points
eta_g = [-1; -1;  1;  1] / sqrt(3); % Local coordindates of Gauss points

for j = 1:4
    Be_g = B_matrix_plate( xi_g(j), eta_g(j) );
    Ke = Ke + transpose(Be_g)*D_b*Be_g; 
end

Ke = Ke*Lx*Ly/4;

%% Local B matrix
    function Be = B_matrix_plate(xi, eta)
        Be = zeros(3,12);

        % Block for node #1
        Be(:, 1:3) = ...
            [ 3/(Lx*Lx)*xi*(eta-1), 0, 1/(2*Lx)*(1-3*xi)*(eta-1); 
              3/(Ly*Ly)*eta*(xi-1), 1/(2*Ly)*(3*eta-1)*(xi-1), 0;
              1/(Lx*Ly)*(3*xi*xi + 3*eta*eta - 4), ...
              1/(2*Lx)*(3*eta*eta - 2*eta - 1), ...
              1/(2*Ly)*(1 + 2*xi - 3*xi*xi) ];

        % Block for node #2
        Be(:, 4:6) = ...
            [ 3/(Lx*Lx)*xi*(1-eta), 0, 1/(2*Lx)*(1+3*xi)*(1-eta); 
             -3/(Ly*Ly)*eta*(xi+1), 1/(2*Ly)*(1-3*eta)*(xi+1), 0;
              1/(Lx*Ly)*(4 - 3*xi*xi - 3*eta*eta), ...
              1/(2*Lx)*(1 + 2*eta - 3*eta*eta ), ...
              1/(2*Ly)*(1 - 2*xi - 3*xi*xi) ];

        % Block for node #3
        Be(:, 7:9) = ...
            [ 3/(Lx*Lx)*xi*(1+eta),  0, 1/(2*Lx)*(1+3*xi)*(1+eta); 
              3/(Ly*Ly)*eta*(xi+1), -1/(2*Ly)*(1+3*eta)*(xi+1), 0;
              1/(Lx*Ly)*(3*xi*xi + 3*eta*eta - 4), ...
              1/(2*Lx)*(1 - 2*eta - 3*eta*eta ), ...
              1/(2*Ly)*(3*xi*xi + 2*xi - 1) ];

        % Block for node #3
        Be(:, 10:12) = ...
            [-3/(Lx*Lx)*xi*(1+eta), 0, 1/(2*Lx)*(3*xi-1)*(1+eta); 
              3/(Ly*Ly)*eta*(1-xi), 1/(2*Ly)*(1+3*eta)*(xi-1), 0;
              1/(Lx*Ly)*(4 - 3*xi*xi - 3*eta*eta ), ...
              1/(2*Lx)*(3*eta*eta + 2*eta - 1 ), ...
              1/(2*Ly)*(3*xi*xi - 2*xi - 1) ];
    end
end