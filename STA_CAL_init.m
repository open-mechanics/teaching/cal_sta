folder = "132sta/"; % Target folder for the source files
repo = "https://gitlab.com/open-mechanics/teaching/cal_sta/-/raw/main/"; 

files = { ...
    "element_forces_truss.m";
    "stiffness_matrix_truss.m"; 
    "lecture_04.mlx" 
    "element_forces_frame.m";
    "stiffness_matrix_frame.m"; 
    "lecture_05.mlx";
    "solve_partitioned_system.m";
    "node_numbers_to_ids.m";
    "stiffness_matrix_elasticity.m";
    "stresses_elasticity.m";
    "lecture_08.mlx";
    "element_forces_plates.m";
    "stiffness_matrix_plate.m";
    "lecture_10.mlx";
    };

%% Delete/create the target folder
if( isfolder( folder ) )
    rmdir( folder, "s" );
end

mkdir( folder );

%% Download files
for i = 1:length(files)
    websave( ...
        strcat(folder, files{i}), strcat(repo, files{i}) );
end

clear all;
