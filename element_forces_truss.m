function fe_l = element_forces_truss(Ee, Ae, xz_a, xz_b, de)

%% Geometry
Dxz = xz_b - xz_a; % Coordinate differences
Le = norm(Dxz); % Element length
c = Dxz(1) / Le; s = Dxz(2) / Le; % Directional cosines

%% End forces in truss
Be = [-c -s c s ]; % Geometrical matrix
fe_l = Ee*Ae/Le*Be*de; % Element end forces

end