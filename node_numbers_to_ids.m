function ID = node_numbers_to_ids( node_nums, dofs_per_node )

num_nodes = size(node_nums, 2);

ID = dofs_per_node*repelem(node_nums, dofs_per_node) - ...
    repmat((dofs_per_node:-1:1) - 1, [1, num_nodes]);

end