function Ke = stiffness_matrix_truss(Ee, Ae, xz_a, xz_b)

%% Geometry
Dxz = xz_b - xz_a; % Coordinate differences
Le = norm(Dxz); % Element length
c = Dxz(1) / Le; s = Dxz(2) / Le; % Directional cosines

%% Stiffness matrix

Ke = Ee*Ae/Le * ...
    [ c*c,  c*s, -c*c, -c*s; ...
      c*s,  s*s, -c*s, -s*s; ...
     -c*c, -c*s,  c*c,  c*s; ...
     -c*s, -s*s,  c*s,  s*s ];
end