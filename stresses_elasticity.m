function sigmae = stresses_elasticity(Ee, nue, xye, de)

%% Geometry
Lx = xye(2,1) - xye(1,1);
Ly = xye(3,2) - xye(2,2);
A = Lx * Ly;

%% Material stiffness matrix
De = Ee/(1-nue^2)*[   1 nue         0
                    nue   1         0
                      0   0 (1-nue)/2 ];
%% B matrix in element center
Be = 1/(2*A)*[ -Ly   0  Ly   0 Ly  0 -Ly   0 
                0 -Lx   0 -Lx  0 Lx   0  Lx 
              -Lx -Ly -Lx  Ly Lx Ly  Lx -Ly ];
%% Stress in the element center
sigmae = De*Be*de; 
end