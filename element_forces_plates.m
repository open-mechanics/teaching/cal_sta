function me = element_forces_plates(Ee, nue, xye, te, de)

%% Geometry
Lx = xye(2,1) - xye(1,1);
Ly = xye(3,2) - xye(2,2);

%% Bending stiffness matrix
D_b = Ee*te^3/(12*(1 - nue^2)) * ...
    [  1, nue,         0; 
      nue,  1,         0; 
        0,  0, (1-nue)/2];
%% Specific moments at the element center
me = D_b*B_matrix_plate(0, 0)*de;

%% Local B matrix
    function Be = B_matrix_plate(xi, eta)
        Be = zeros(3,12);

        % Block for node #1
        Be(:, 1:3) = ...
            [ 3/(Lx*Lx)*xi*(eta-1), 0, 1/(2*Lx)*(1-3*xi)*(eta-1); 
              3/(Ly*Ly)*eta*(xi-1), 1/(2*Ly)*(3*eta-1)*(xi-1), 0;
              1/(Lx*Ly)*(3*xi*xi + 3*eta*eta - 4), ...
              1/(2*Lx)*(3*eta*eta - 2*eta - 1), ...
              1/(2*Ly)*(1 + 2*xi - 3*xi*xi) ];

        % Block for node #2
        Be(:, 4:6) = ...
            [ 3/(Lx*Lx)*xi*(1-eta), 0, 1/(2*Lx)*(1+3*xi)*(1-eta); 
             -3/(Ly*Ly)*eta*(xi+1), 1/(2*Ly)*(1-3*eta)*(xi+1), 0;
              1/(Lx*Ly)*(4 - 3*xi*xi - 3*eta*eta), ...
              1/(2*Lx)*(1 + 2*eta - 3*eta*eta ), ...
              1/(2*Ly)*(1 - 2*xi - 3*xi*xi) ];

        % Block for node #3
        Be(:, 7:9) = ...
            [ 3/(Lx*Lx)*xi*(1+eta),  0, 1/(2*Lx)*(1+3*xi)*(1+eta); 
              3/(Ly*Ly)*eta*(xi+1), -1/(2*Ly)*(1+3*eta)*(xi+1), 0;
              1/(Lx*Ly)*(3*xi*xi + 3*eta*eta - 4), ...
              1/(2*Lx)*(1 - 2*eta - 3*eta*eta ), ...
              1/(2*Ly)*(3*xi*xi + 2*xi - 1) ];

        % Block for node #3
        Be(:, 10:12) = ...
            [-3/(Lx*Lx)*xi*(1+eta), 0, 1/(2*Lx)*(3*xi-1)*(1+eta); 
              3/(Ly*Ly)*eta*(1-xi), 1/(2*Ly)*(1+3*eta)*(xi-1), 0;
              1/(Lx*Ly)*(4 - 3*xi*xi - 3*eta*eta ), ...
              1/(2*Lx)*(3*eta*eta + 2*eta - 1 ), ...
              1/(2*Ly)*(3*xi*xi - 2*xi - 1) ];
    end
end