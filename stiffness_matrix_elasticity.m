function Ke = stiffness_matrix_elasticity(Ee, nue, xye, te)

%% Geometry
Lx = xye(2,1) - xye(1,1);
Ly = xye(3,2) - xye(2,2);

assert( abs(Lx-Ly) < 10*eps, ...
    'Implementation available only for square elements' )

k=[ 1/2-nue/6   1/8+nue/8 -1/4-nue/12 -1/8+3*nue/8 ... 
   -1/4+nue/12 -1/8-nue/8  nue/6       1/8-3*nue/8 ];

Ke = Ee/(1-nue^2)*[ k(1) k(2) k(3) k(4) k(5) k(6) k(7) k(8)
                    k(2) k(1) k(8) k(7) k(6) k(5) k(4) k(3)
                    k(3) k(8) k(1) k(6) k(7) k(4) k(5) k(2)
                    k(4) k(7) k(6) k(1) k(8) k(3) k(2) k(5)
                    k(5) k(6) k(7) k(8) k(1) k(2) k(3) k(4)
                    k(6) k(5) k(4) k(3) k(2) k(1) k(8) k(7)
                    k(7) k(4) k(5) k(2) k(3) k(8) k(1) k(6)
                    k(8) k(3) k(2) k(5) k(4) k(7) k(6) k(1) ]*te;
end