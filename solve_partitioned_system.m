function [dU, R] = solve_partitioned_system(K, d, f, U, C)

dU = K(U, U) \ (f(U) - K(U, C)*d(C)); % Unconstrained degrees of freedom
R  = K(C, U)*dU + K(C, C)*d(C) - f(C); % Reactions
end